# Convy

Stack:
- docker 19.x
- nodejs 10.x
- postgres 11.x

## Buckets

0. `rm -rf node_modules`
1. `cp .env.dist .env`
2. `docker-compose up -d`
3. `docker-compose run --rm convy-api npx sequelize db:migrate`

## Local Tunnel
3. `docker-compose logs convy-tunnel` (to find local tunnel)

## Dialogflow

0. Place a dialogflow authentication key in the root of the project: https://dialogflow.com/docs/reference/v2-auth-setup#getting_the_service_account_key
1. Change the corresponding .env var