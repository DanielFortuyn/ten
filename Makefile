.PHONY:

down:
	docker-compose down

up:
	docker-compose up -d

pull:
	docker-compose pull

up-build: pull
	docker-compose up -d --build

up-log: up
	docker-compose logs -f

reload: down up

rebuild: down up-build

seed:
	docker-compose exec ten-api npx sequelize-cli db:seed:all

migrate-down:
	docker-compose exec ten-api npx sequelize-cli db:migrate:undo:all

migrate-up:
	docker-compose exec ten-api npx sequelize-cli db:migrate

db-drop:
	docker-compose exec ten-api npx sequelize-cli db:drop

db-create:
	docker-compose exec ten-api npx sequelize-cli db:create

db-reset: db-drop db-create

reset: db-reset migrate-up seed

logs:
	docker-compose logs -f ten-api

test:
	docker-compose exec ten-api npm run test