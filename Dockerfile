FROM node:10-alpine as build-base
RUN apk add --no-cache build-base python g++

FROM build-base as deps
ARG NPM_CMD="npm ci --only=production"
WORKDIR /deps
COPY package.json package-lock.json ./
RUN ${NPM_CMD}

FROM node:10-alpine as dev
WORKDIR /usr/src/app
COPY --from=deps /deps .
COPY . .
CMD ["npm", "run", "start"]

FROM build-base as build
WORKDIR /build
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM node:10-alpine as prod
WORKDIR /usr/src/app
RUN mkdir dist
COPY --from=build /build/dist ./dist
COPY --from=deps /deps .
COPY ${GOOGLE_APPLICATION_CREDENTIALS} .sequelizerc ./
CMD ["npm", "run", "serve"]
