'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    //   let ids = [
    //     uuid.v4(),
    //     uuid.v4(),
    //     uuid.v4(),
    //     uuid.v4()
    //   ];
    //   let phases = [
    //    1
    //   ]
    //   let phase = await queryInterface.bulkInsert('Phases', [
    //     {
    //       id: phases[0]++,
    //       name: 'Start',
    //       code: '001START',
    //       teamId: 0,
    //       lat: 50.754446,
    //       lng: 6.020887,
    //       range: 100,
    //       createdAt: new Date(),
    //       updatedAt: new Date()
    //     },
    //     {
    //       id: phases[0]++,
    //       name: 'Hoogmade',
    //       code: 'Hoogmade',
    //       lat: 52.169125,
    //       lng: 5.583374,
    //       teamId: 0,
    //       range: 100,
    //       createdAt: new Date(),
    //       updatedAt: new Date()
    //     }
    //   ]);
    //   console.log(phase);
    //   let teams = await queryInterface.bulkInsert('Teams', [{
    //     id: ids[0],
    //     appId: process.env.APP_ID,
    //     deviceId: '9790c09c45bea900',
    //     name: 'Team F',
    //     slug: 'f',
    //     image: 'assets/blue.png',
    //     phaseId: phases[0],
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },{
    //     id: ids[1],
    //     appId: process.env.APP_ID,
    //     deviceId: '1e3abd585af530932',
    //     name: 'Snorro',
    //     slug: 'c',
    //     image: 'assets/purple.png',
    //     phaseId: phases[0],
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },{
    //     id: ids[2],
    //     appId: process.env.APP_ID,
    //     deviceId: '909ff9f4b47d9068',
    //     name: 'Roverheid',
    //     slug: 'p',
    //     image: 'assets/green.png',
    //     phaseId: phases[0],
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },{
    //     id: ids[3],
    //     appId: process.env.APP_ID,
    //     deviceId: 'e3abd585af530932',
    //     name: 'Remspoor',
    //     slug: 'R',
    //     image: 'assets/brown.png',
    //     phaseId: phases[0],
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   }]);
    //   let memberId = [
    //     uuid.v4(),
    //     uuid.v4(),
    //     uuid.v4(),
    //     uuid.v4()
    //   ];
    //   let teamMembers = await queryInterface.bulkInsert('TeamMembers', [{
    //     id: memberId[0],
    //     teamId: ids[0],
    //     role: "admin",
    //     firstName: "Daniel",
    //     lastName: "Fortuyn",
    //     nick: "Fortuyn",
    //     slug: "df",
    //     image: "",
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },{
    //     id: memberId[1],
    //     teamId: ids[2],
    //     role: "admin",
    //     firstName: "Joost",
    //     lastName: "Wensveen",
    //     nick: "De Penus",
    //     slug: "jw",
    //     image: "",
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },{
    //     id: memberId[2],
    //     teamId: ids[1],
    //     role: "admin",
    //     firstName: "Tommie",
    //     lastName: "Overkamp",
    //     nick: "Mein Kamper",
    //     slug: "to",
    //     image: "",
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },
    //   {
    //     id: memberId[3],
    //     teamId: ids[1],
    //     role: "",
    //     firstName: "Daan",
    //     lastName: "Schoone",
    //     nick: "Schoonheid",
    //     slug: "ds",
    //     image: "",
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   }
    // ]);
    // let scores = await queryInterface.bulkInsert('Scores', [{
    //   id: uuid.v4(),
    //   teamMemberId: memberId[0],
    //   score: 10,
    //   description: 'Organizing the trip',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },{
    //   id: uuid.v4(),
    //   teamMemberId: memberId[1],
    //   score: 10,
    //   description: 'Organizing the trip',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },{
    //   id: uuid.v4(),
    //   teamMemberId: memberId[1],
    //   score: 10,
    //   description: 'Organizing the trip',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },
    // {
    //   id: uuid.v4(),
    //   teamMemberId: memberId[2],
    //   score: 9,
    //   description: 'Organizing the trip',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },
    // {
    //   id: uuid.v4(),
    //   teamMemberId: memberId[3],
    //   score: -1,
    //   description: 'No contact',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // }]
    // );

    let codes = await queryInterface.bulkInsert('Codes',
      [
        {
          name: 'Testcode, nice',
          code: 'test',
          score: -1,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Brief qr',
          code: '96goedevondst69',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Bedankje',
          code: 'bedanktroverheid',
          score: -6,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Nog één',
          code: 'bedanktfortuyn',
          score: -4,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Kon het niet laten..',
          code: 'grieksefascist',
          score: -7,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'idem',
          code: 'fortuynheld',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'konami',
          code: 'upupdowndownleftrightleftrightba',
          score: -15,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Straf',
          code: 'nietgeluisterd',
          score: 3,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Straf',
          code: 'dommevraag',
          score: 5,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Straf',
          code: 'deutschland',
          score: -4,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Straf',
          code: 'echteendommevraag',
          score: 10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'RSA',
          code: 'ditiseenwerkendecode',
          score: -12,
          unique: true,
          once: true,
          self: true
        },
        {
          code: 'compensatieminuut',
          score: -1,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Uiteraard..',
          code: '101010',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Uiteraard..',
          code: '101010',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Motorlampje..',
          code: 'motorlampje',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          name: 'Lekke ban..',
          code: 'lekkeband',
          score: -10,
          unique: true,
          once: true,
          self: true
        },
        {
          "name": "Eerste Big Mac geklapt ",
          "score": -10,
          "unique": true,
          "once": true,
          "code": "BigMc1",
          "self": false
        },
        {
          "name": "Eerste Kanon verorberd",
          "score": -10,
          "unique": true,
          "once": true,
          "code": "GrlsKan1",
          "self": false
        },
        {
          "name": "Vrouwelijke lifter meenemen ",
          "score": -20,
          "unique": false,
          "once": true,
          "code": "CixtbZ",
          "self": false
        },
        {
          "name": "Mannelijke lifter mee in de auto ",
          "score": -5,
          "unique": false,
          "once": true,
          "code": "R2AvAm",
          "self": false
        },
        {
          "name": "Neuken op de achterbank ",
          "score": -60,
          "unique": false,
          "once": false,
          "code": "MRUUzW",
          "self": false
        },
        {
          "name": "Geneukt worden op de achterbank ",
          "score": -6,
          "unique": false,
          "once": false,
          "code": "EnJl2Y",
          "self": false
        },
        {
          "name": "Geld verdienen door Blablacar",
          "score": -30,
          "unique": false,
          "once": true,
          "code": "eTi9QE",
          "self": false
        },
        {
          "name": "Tatoeage van lustrumlogo laten zetten ",
          "score": -50,
          "unique": false,
          "once": false,
          "code": "jQBbbn",
          "self": false
        },
        {
          "name": "Eerste auto met motorpech ",
          "score": -120,
          "unique": true,
          "once": true,
          "code": "J1ThPW",
          "self": false
        },
        {
          "name": "De wagen blauw laten staan van de vape",
          "score": -15,
          "unique": false,
          "once": true,
          "code": "VapeItBby",
          "self": false
        },
        {
          "name": "Chick neuken ((bmi / 100 ) / (leeftijd * 24) + cupmaat tabel ",
          "score": -100,
          "unique": false,
          "once": false,
          "code": "T2Uuys",
          "self": false
        },
        {
          "name": "Voor Pensveen if Asian maximaal",
          "score": -10,
          "unique": false,
          "once": false,
          "code": "sadfsdf",
          "self": false
        },
        {
          "name": "Voor Tommie, hoertje maximaal ",
          "score": -50,
          "unique": false,
          "once": false,
          "code": "8hVTTI",
          "self": false
        },
        {
          "name": "Aantal dispuutsitems in de auto ",
          "score": -3,
          "unique": false,
          "once": false,
          "code": "aaayBV",
          "self": false
        },
        {
          "name": "Beste speech tijdens een diner",
          "score": -15,
          "unique": true,
          "once": true,
          "code": "yYTzme",
          "self": false
        },
        {
          "name": "Organiseren van de reis",
          "score": -20,
          "unique": true,
          "once": true,
          "code": "SzxRiP",
          "self": false
        },
        {
          "name": "Ander team boos laten wegrijden",
          "score": -30,
          "unique": false,
          "once": false,
          "code": "RsX2sR",
          "self": false
        },
        {
          "name": "Individueel boos wegrijden",
          "score": 30,
          "unique": false,
          "once": false,
          "code": "lGO6rX",
          "self": false
        },
        {
          "name": "Team boos wegrijden",
          "score": 55,
          "unique": false,
          "once": false,
          "code": "6f2E49",
          "self": false
        },
        {
          "name": "Boos achtergelaten worden",
          "score": -10,
          "unique": false,
          "once": false,
          "code": "PEl4Av",
          "self": false
        },
        {
          "name": "Een etappe afleggen met een auto van een ander team",
          "score": -60,
          "unique": false,
          "once": false,
          "code": "UiRFIA",
          "self": false
        },
        {
          "name": "Niet gebruik maken van de geregelde overnachting ",
          "score": -30,
          "unique": false,
          "once": false,
          "code": "8t6aIz",
          "self": false
        },
        {
          "name": "Als eerste aangehouden worden door de politie",
          "score": -60,
          "unique": true,
          "once": true,
          "code": "N1WJ1k",
          "self": false
        },
        {
          "name": "Bekeuring ontvangen tijdens de reis",
          "score": -60,
          "unique": false,
          "once": false,
          "code": "HrKrXT",
          "self": false
        },
        {
          "name": "Jailtime",
          "score": -300,
          "unique": false,
          "once": true,
          "code": "kV0FM7",
          "self": false
        },
        {
          "name": "Eerst roadkill maken",
          "score": -15,
          "unique": true,
          "once": true,
          "code": "Rdkill1",
          "self": false
        },
        {
          "name": "Sponsoring voor je team regelen (per sponsor)",
          "score": -15,
          "unique": false,
          "once": false,
          "code": "RBZUyX",
          "self": false
        },
        {
          "name": "Beste dronken speech",
          "score": -30,
          "unique": true,
          "once": true,
          "code": "Nn6Px1",
          "self": false
        },
        {
          "name": "Beste quote",
          "score": -10,
          "unique": true,
          "once": true,
          "code": "6MPJSi",
          "self": false
        },
        {
          "name": "Land bezocht (per land) ",
          "score": -15,
          "unique": false,
          "once": false,
          "code": "E7qJoS",
          "self": false
        },
        {
          "name": "Meer dan 200 km/h rijden ",
          "score": -30,
          "unique": false,
          "once": true,
          "code": "KG08IX",
          "self": false
        },
        {
          "name": "Sponsoren van uitoefenaars van het oudste beroep",
          "score": -30,
          "unique": false,
          "once": true,
          "code": "h0uqRh",
          "self": false
        },
        {
          "name": "Drugs kopen (alleen buiten reisgezelschap)",
          "score": -20,
          "unique": false,
          "once": false,
          "code": "pqWFGh",
          "self": false
        },
        {
          "name": "Drugs verkopen (alleen buiten reisgezelschap)",
          "score": -25,
          "unique": false,
          "once": false,
          "code": "qKkgde",
          "self": false
        },
        {
          "name": "Harddrugs gebruiken tijdens een etappe",
          "score": -15,
          "unique": false,
          "once": false,
          "code": "BQOJOg",
          "self": false
        },
        {
          "name": "Met je hele  team op de top van een berg (>1500m hoog)",
          "score": -60,
          "unique": false,
          "once": true,
          "code": "WbjVTk",
          "self": false
        },
        {
          "name": "Item van Dirk mee op reis",
          "score": -15,
          "unique": false,
          "once": false,
          "code": "bPT83m",
          "self": false
        },
        {
          "name": "Dirk mee op reis",
          "score": -100,
          "unique": true,
          "once": true,
          "code": "KMuDnI",
          "self": false
        },
        {
          "name": "Naked driving ",
          "score": -10,
          "unique": false,
          "once": true,
          "code": "zGPTMv",
          "self": false
        },
        {
          "name": "Team naked driving",
          "score": -15,
          "unique": false,
          "once": true,
          "code": "ayJF74",
          "self": false
        },
        {
          "name": "Ontbijt verzorgen",
          "score": -10,
          "unique": false,
          "once": false,
          "code": "wfFB8l",
          "self": false
        },
        {
          "name": "Iemand keren",
          "score": -11,
          "unique": false,
          "once": false,
          "code": "KnKteC",
          "self": false
        },
        {
          "name": "Barfen uit het raam",
          "score": -7.4,
          "unique": false,
          "once": false,
          "code": "dQkwqr",
          "self": false
        },
        {
          "name": "Wegrijden met een andere auto",
          "score": -15,
          "unique": false,
          "once": false,
          "code": "0uoSbU",
          "self": false
        },
        {
          "name": "Geneukt worden door een dispuutsgenoot",
          "score": 100,
          "unique": false,
          "once": false,
          "code": "mwZ2XN",
          "self": false
        },
        {
          "name": "Gepijpt worden door Fred",
          "score": -1,
          "unique": false,
          "once": false,
          "code": "3FYR0Q",
          "self": false
        },
        {
          "name": "Vrijwillig gepijpt worden door Fred",
          "score": -2,
          "unique": false,
          "once": false,
          "code": "4SeWy7",
          "self": false
        },
        {
          "name": "Bij de Roverheid melden dat er iets mis is met de app",
          "score": 15,
          "unique": false,
          "once": true,
          "code": "ur0roM",
          "self": false
        },
        {
          "name": "De Roverheid storen tijdens het Roverheid overleg ",
          "score": 5,
          "unique": false,
          "once": false,
          "code": "WZKOf9",
          "self": false
        },
        {
          "name": "Hackpogingen op de app",
          "score": 15,
          "unique": false,
          "once": false,
          "code": "NBubF9",
          "self": false
        },
        {
          "name": "Laffe hackpoging op de app",
          "score": 10,
          "unique": false,
          "once": false,
          "code": "DNukZU",
          "self": false
        },
        {
          "name": "Succesvolle hackpoging op de app",
          "score": -100,
          "unique": true,
          "once": true,
          "code": "M3yGp5",
          "self": false
        },
        {
          "name": "Domme vraag stellen",
          "score": 10,
          "unique": false,
          "once": false,
          "code": "rzy9l3",
          "self": false
        },
        {
          "name": "Domme vraag stellen terwijl je redelijkerwijs had kunnen weten dat het een domme vraag is",
          "score": 15,
          "unique": false,
          "once": false,
          "code": "s5lsm1",
          "self": false
        },
        {
          "name": "Activiteit 1 winnen",
          "score": -50,
          "unique": true,
          "once": true,
          "code": "Y6rLQP",
          "self": false
        },
        {
          "name": "Beste eindpresentatie",
          "score": -100,
          "unique": true,
          "once": true,
          "code": "bGacB0",
          "self": false
        },
        {
          "name": "Hoogste topsnelheid",
          "score": -20,
          "unique": true,
          "once": true,
          "code": "H61hMi",
          "self": false
        },
        {
          "name": "Minste benzine gebruikt",
          "score": -20,
          "unique": true,
          "once": true,
          "code": "sCL1hs",
          "self": false
        },
        {
          "name": "Meeste domme vragen gesteld tijdens de reis",
          "score": 10,
          "unique": true,
          "once": true,
          "code": "xSdF1w"
        }
      ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Teams',
      null,
      {});
    return queryInterface.bulkDelete('Scores',
      null,
      {});
  }
};