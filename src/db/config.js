const { URL } = require("url");
const POSTGRES = "postgres";

const urlParser = dbUrl => {
  if (!dbUrl) return {};

  const { pathname, username, password, hostname: host, port } = new URL(dbUrl);

  return {
    username,
    password,
    host,
    port,
    database: pathname.substring(1),
    dialect: POSTGRES,
    protocol: POSTGRES
  };
};

let urlConfig;

if (process.env.DATABASE_URL) {
  urlConfig = urlParser(process.env.DATABASE_URL);
}

const config = {
  username: process.env.DB_USER || "ten",
  password: process.env.DB_PASSWORD || null,
  database: process.env.DB_NAME || "ten_development",
  host: process.env.DB_HOST || "ten-db",
  port: process.env.DB_PORT || 5432,
  logging: false,
  dialect: "postgres"
};

module.exports = urlConfig || config;
