'use strict';
module.exports = (sequelize, DataTypes) => {
  const Code = sequelize.define('Code', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    once: DataTypes.BOOLEAN,
    score: DataTypes.INTEGER
  }, {});
  Code.associate = function (models) {
    // associations can be defined here
  };
  return Code;
};