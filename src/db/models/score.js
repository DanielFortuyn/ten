'use strict';
module.exports = (sequelize, DataTypes) => {
  const Score = sequelize.define('Score', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    teamId: DataTypes.STRING,
    source: DataTypes.STRING,
    score: DataTypes.FLOAT,
    description: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  Score.associate = function (models) {
    // associations can be defined here
  };
  return Score;
};