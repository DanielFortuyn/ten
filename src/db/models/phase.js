'use strict';
module.exports = (sequelize, DataTypes) => {
  const Phase = sequelize.define('Phase', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    order: DataTypes.INTEGER,
    name: DataTypes.STRING,
    message: DataTypes.TEXT,
    isoMode: DataTypes.BOOLEAN,
    mapType: DataTypes.STRING,
    marker: DataTypes.STRING,
    lat: DataTypes.FLOAT,
    lng: DataTypes.FLOAT,
    teamId: DataTypes.INTEGER,
    range: DataTypes.FLOAT,
    slug: DataTypes.STRING,
    completed: DataTypes.BOOLEAN,
    gameId: DataTypes.INTEGER,
    code: DataTypes.STRING,
    nextPhaseId: DataTypes.INTEGER,
    imageUrl: DataTypes.STRING,
    config: DataTypes.JSON
  }, {});
  Phase.associate = function (models) {
    // associations can be defined here
  };
  return Phase;
};