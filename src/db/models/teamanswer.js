'use strict';
module.exports = (sequelize, DataTypes) => {
  const TeamAnswer = sequelize.define('TeamAnswer', {
    id: {
      type: DataTypes.STRING,
      primaryKey:true,
    },
    teamId: DataTypes.STRING,
    answer: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  TeamAnswer.associate = function(models) {
    // associations can be defined here
  };
  return TeamAnswer;
};