'use strict';
module.exports = (sequelize, DataTypes) => {
  const Team = sequelize.define('Team', {
    id: {
      type: DataTypes.STRING,
      primaryKey:true,
    },
    appId: DataTypes.STRING,
    deviceId: DataTypes.STRING,
    slug: DataTypes.STRING,
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    phaseId: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  Team.associate = function(models) {
    Team.hasOne(models.Phase, {foreignKey: 'id', sourceKey: 'phaseId'});
  };
  return Team;
};