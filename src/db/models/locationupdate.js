'use strict';
module.exports = (sequelize, DataTypes) => {
  const LocationUpdate = sequelize.define('LocationUpdate', {
    id: {
      type: DataTypes.STRING,
      primaryKey:true,
    },
    appId: DataTypes.STRING,
    deviceId: DataTypes.STRING,
    lat: DataTypes.FLOAT,
    lng: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  LocationUpdate.associate = function(models) {
    // associations can be defined here
  };
  return LocationUpdate;
};