import models from "../../db/models";
import uuid from "uuid";
const fs = require('fs');

module.exports = async function (req, res, next) {
    try {
        var date = new Date();
        if(req.body.image) {
            fs.writeFile("storage/" + date.getTime() + ".jpg", new Buffer(req.body.image, "base64"), function(err) {
                if(err) {
                    console.log(err);
                }
                console.log('Written to ');
            })
        }
        let teams = await models.TeamAnswer.create({
            id: uuid.v4(),
            answer: req.body.answer || 'not supplied',
            teamId: req.body.teamId || 'not supplied',
            createdAt: new Date(),
            updatedAt: new Date()
        });
        res.send(204);
    } catch(e) {
        console.log(e);
    }
    next();
};