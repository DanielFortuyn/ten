import uuid from "uuid";
import models from "../../db/models";
import score from "../../helpers/score";
import Sequelize from "sequelize";
import * as scoreclass from '../../helpers/score';

module.exports = async function (req, res, next) {
    const { deviceId } = req.body;
    console.log(`========================> PHASE BY CODE ${deviceId}  <========================`);

    let code = req.body.code.substring(0, 100).toLowerCase().replace(/[^a-z0-9-]/g, '');
    let team = await models.Team.findOne({ where: { deviceId: deviceId } });

    let codeindb = await models.Code.findOne({ where: { code: code, self: true } });
    if (codeindb) {
        if (await scoreclass.handleCode(codeindb, team, true) != 404) {
            return res.send(204);
        }
    }

    //Evt phase code
    let phase = await models.Phase.findOne({ where: { code: { [Sequelize.Op.iLike]: code } } });
    if (phase) {
        if (team) {
            await team.update({
                phaseId: phase.id
            });
            score.score(team.id, 'code', "Team entered code: " + code);
            return res.send(204);
        }
    } else {
        score.score(team.id, 'codefail', "Team entered code: " + code, 1);
    }
    return res.send(404);
};