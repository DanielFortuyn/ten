import uuid from "uuid";
import models from "../../db/models";
import Sequelize from "sequelize";
import crypto from 'crypto';

module.exports = async function (req, res, next) {

    let namePrefix = 'etappe_2';

    for (let i = 30000; i < 30030; i++) {
        models.Phase.create({

            id: i,
            order: 0,
            teamId: 0,
            name: namePrefix + i,
            message: namePrefix + i,
            marker: 'circle',
            isoMode: true,
            lat: 52.5,
            lng: 5,
            range: 700,
            slug: namePrefix + i,
            completed: false,
            gameId: 1,
            code: crypto.createHash('md5').update(namePrefix + i).digest('hex').substring(0, 6),
            nextPhaseId: i + 1,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            mapType: 'normal',
            imageUrl: ''
        });
    }

    return res.send(404);
};