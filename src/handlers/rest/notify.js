import models from "../../db/models";
var admin = require("firebase-admin");
const uuid = require('uuid');
module.exports = async function (req, res, next) {

    const { body } = req;

    if (body.pw == "5M83ftUfsEDmvuOEaI5Q7Bxx25c3P65jId7pBv") {

        let topic = 'updates';
        //Geen phase id geen phase bump
        if (typeof req.body.phaseId != 'undefined') {
            if (typeof req.body.teamId != 'undefined') {
                let team = await models.Team.findOne({ where: { id: req.body.teamId }, include: models.Phase });
                if (team) {
                    team.update({
                        phaseId: req.body.phaseId || 20
                    });
                    topic = 'device.' + team.deviceId;
                }
                console.log(`Phase bumped team ${req.body.teamId}`);
            } else {
                //Update all teams
                models.Team.findAll({ include: models.Phase }).then((teams) => {
                    for (let i = 0; i < teams.length; i++) {
                        let team = teams[i];
                        team.update({
                            phaseId: req.body.phaseId || 20
                        });
                    }
                });
                console.log('Phase bumped all teams..');
            }
        } else {
            console.log('No phase bump..');
        }
        let eventData = {
            lat: req.body.lat || "52.157237",
            lng: req.body.lng || "4.494089",
            "dialog.show": req.body.show || "false",
            "dialog.title": req.body.dialogTitle || "Title",
            "dialog.content": req.body.dialogContent || "",
            "dialog.subtitle": req.body.dialogSubtitle || ""
        };

        models.Event.create({
            id: uuid.v4(),
            teamId: req.body.teamId,
            event: req.body.event || 'place.finish',
            data: eventData,
        });



        var payload = {
            notification: {
                clickAction: "FLUTTER_NOTIFICATION_CLICK",
                title: req.body.title || "Zoek",
                body: req.body.body || "Nieuwe opdracht toegevoegd aan de race!",
            },
            data: eventData
        };

        var options = {
            priority: "high",
            timeToLive: 60 * 60
        };

        console.log('SENDING TO TOPIC.. ' + topic + ' with teamId ' + body.teamId)

        admin.messaging().sendToTopic(topic, payload, options)
            .then(function (response) {
                console.log("Successfully sent message:", response);
                res.send(response);
            })
            .catch(function (error) {
                console.log("Error sending message:", error);
                res.send(error);
            });
    } else {
        res.send(404);
    }

}