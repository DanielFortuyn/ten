import models from "../../db/models";

module.exports = async function (req, res, next) {
    try {
        const { QueryTypes } = require('sequelize');
        let result = await models.sequelize.query('select t.slug, sum(score) as minutes from "Scores" s left join "Teams" t  on (t.id = s."teamId")  group by t.slug', {
            type: QueryTypes.SELECT
        });

        let lengthyResult = await models.sequelize.query('select slug, description, s."source", s.score  from "Scores" s left join "Teams" t on (t.id = s."teamId") order by slug, s."createdAt" desc', {
            type: QueryTypes.select
        });

        let responseObject = {
            summary: result,
            lengthyResult: lengthyResult[0],
        }

        res.send(responseObject);
    } catch (e) {
        console.log(e);
    }
    next();
};