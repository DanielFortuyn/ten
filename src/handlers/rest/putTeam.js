import models from "../../db/models";

module.exports = async function (req, res, next) {
    console.log(req.params);
    console.log(req.body);
  
    models.Team.findOne({where: {id: req.params.id}}).then((team) => {
      team.update({
        deviceId: req.body.deviceId
      });
    });
    return res.send(204);
  };