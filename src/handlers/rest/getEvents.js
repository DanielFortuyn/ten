import models from "../../db/models";

module.exports = async function (req, res, next) {
    try {
        let events = await models.Event.findAll();
        res.send(events);
    } catch(e) {
        console.log(e);
    }
    next();
};