import models from "../../db/models";

module.exports = async function (req, res, next) {
    try {
        let teamAnswers = await models.TeamAnswers.findAll();
        res.send(teamAnswers);
    } catch(e) {
        console.log(e);
    }
    next();
};