const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);
const handlers = {};

fs.readdirSync(__dirname)
  .filter(file => {
    //only js files not equal to current file (index.js)
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(file => {
    //dynamically load module and register it to the dictionary with handlers
    const handlerName = path.basename(file, ".js");
    handlers[handlerName] = require(path.join(__dirname, file));
  });

module.exports = handlers;