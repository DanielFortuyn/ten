import models from "../../db/models";

module.exports = async function (req, res, next) {
    try {
        let teams = await models.Team.findAll({
            include: models.Phase
        });
        res.send(teams);
    } catch(e) {
        console.log(e);
    }
    next();
};