import uuid from "uuid";
import models from "../../db/models";
import score from "../../helpers/score";

module.exports = async function (req, res, next) {

    models.Team.findOne({ where: { deviceId: req.body.deviceId }, include: models.Phase }).then((team) => {
        console.log(`========================> PHASE BUMP ${team.phaseId} <========================`);

        // if (team.Phase.code === req.body.entryId) {
        //Set team to the next phase
        team.update({
            phaseId: team.Phase.nextPhaseId
        });
        //Insert corresponding score
        score.score(team.id, 'phasefromgeo', team.Phase.id);
        // }
    });
    return res.send(204);
};