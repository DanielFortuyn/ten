import models from "../../db/models";
import fence from "../../helpers/fence";
const { QueryTypes } = require('sequelize');


module.exports = async function (req, res, next) {
    let location = await models.sequelize.query(`select id, lat, lng, "createdAt" cat from "LocationUpdates" l where l."deviceId" = '${req.body.deviceId}' order by "createdAt" desc limit 1`, { type: QueryTypes.SELECT });
    let team = await models.Team.findOne({ where: { deviceId: req.body.deviceId }, include: models.Phase });
    location = location[0];

    console.log(`========================> PHASE BUMP MANUAL: ${team.phaseId} FROM: ${req.body.deviceId} <========================`);
    console.log(`found lat:${location.lat} lng:${location.lng}, phase: ${team.phaseId} @ ${team.Phase.lat}, ${team.Phase.lng}`);
    //BUILD GEO FENCE LOGIC
    console.log(location);
    const backyard = [{
        name: 'NAME_OF_FACILITY',
        center: {
            // FLOAT NUMER 00.00000
            latitude: team.Phase.lat,
            // FLOAT NUMBER 00.00000
            longitude: team.Phase.lng,
        },
        // THIS IS METERS OUT FROM THE CENTER POINT
        radius: team.Phase.range
    }];
    if (fence.isInside({
        latitude: location.lat,
        longitude: location.lng,
    }, backyard)
    ) {
        // Still bump phase;
        await team.update({
            phaseId: team.Phase.nextPhaseId
        });
        res.send(204);
        return
    }
    res.send(404);
    return
}


// check is an object { latitude: 11.11111, longitude: 11.11111 }
