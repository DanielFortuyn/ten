import models from "../../db/models";
import * as score from '../../helpers/score';

module.exports = async function (req, res, next) {
    try {
        let { code, team, pass } = req.query;
        code = code.substring(0, 100).toLowerCase().replace(/[^a-z0-9-]/g, '');
        if (code == '' || pass != '42cCT9J1Q7iz') {
            return res.send('input issue');
        }

        let codeindb = await models.Code.findOne({ where: { code: code } });
        let teamindb = await models.Team.findOne({ where: { slug: team } });

        if (codeindb && teamindb) {
            return res.send(await score.handleCode(codeindb, teamindb, false));
        }
        return res.send(404)
    } catch (e) {
        console.log(e);
    }
}



