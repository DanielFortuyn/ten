module.exports = {
    toRadians: function (val) {
        return val * Math.PI / 180
    },
    pointInsideCircle: function (point, circle) {
        const { center } = circle
        const distance = this.distanceBetween(point, center)
        return distance <= circle.radius // Use '<=' if you want to get all points in the border
    },
    distanceBetween: function (point1, point2) {
        const R = 6371e3 // metres
        const φ1 = this.toRadians(point1.latitude)
        const φ2 = this.toRadians(point2.latitude)
        const Δφ = this.toRadians(point2.latitude - point1.latitude)
        const Δλ = this.toRadians(point2.longitude - point1.longitude)

        const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ / 2) * Math.sin(Δλ / 2)

        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return R * c
    },

    isInside: function (check, dataPoints) {
        const TEMP = []
        for (const point in dataPoints) {
            TEMP.push(this.pointInsideCircle(check, { center: dataPoints[point].center, radius: dataPoints[point].radius }))
        }
        const LOOK = TEMP.includes(true)
        return LOOK
    }
}