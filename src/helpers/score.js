import uuid from "uuid";
import models from "../db/models";

module.exports = {
    score: async function (teamId, source, _description, score = null) {
        let pScore = await models.Score.findOne({ where: { teamId: teamId }, order: [['createdAt', 'DESC']] });
        if (pScore != null) {
            console.log('Scoring for ' + source + ' by ' + teamId + ' with pscore ' + pScore.id);
            if (score == null) {
                score = Math.round((((new Date() - pScore.createdAt) % 86400000) % 3600000) / 60000)
            }
        }
        models.Score.create({
            id: uuid.v4(),
            description: _description,
            teamId: teamId,
            source: source,
            score: score
        });
    },
    createScoreFromCodeTeam(_code, _team) {
        models.Score.create({
            id: uuid.v4(),
            description: _code.code,
            teamId: _team.id,
            source: 'qr',
            score: _code.score
        });
    },
    async handleCode(_code, _team) {
        //Unique boeit niet, zo vaak scoren als mogelijk
        if (!_code.once) {
            this.createScoreFromCodeTeam(_code, _team);
            return 'Can be scored all the time...';
        } else {
            //Bouw query om te checken..
            let where = {
                source: 'qr',
                description: _code.code
            }
            if (!_code.unique) {
                where['teamId'] = _team.id;
            }
            //Fetch the scores
            let scores = await models.Score.findAll({ where: where });
            if (scores.length > 0) {
                //Iets gevonden, mag ie maar een keer
                if (!_code.once) {
                    console.log('Found score but can be rescored.. ' + _code.code);
                    this.createScoreFromCodeTeam(_code, _team);
                    return 'Can be scored multiple times.. ' + _code.unique + ' ' + _code.once;
                }
            } else {
                this.createScoreFromCodeTeam(_code, _team);
                return 'First time...';
            }
        }
        return 404;
    }
}