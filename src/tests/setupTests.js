import { load } from "./fixtures";
import db from "../db/models";

const sequelize = db.sequelize;


sequelize.drop({force: true}).then(() => {
  sequelize.sync().then(() => {
    load().then(() => {
      run();
    });
  });
});