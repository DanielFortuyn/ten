import restify from "restify";
import corsMiddleware from "restify-cors-middleware";
import jwt from "restify-jwt-community";
import models from "./db/models";
import uuid from "uuid";
import rest from './handlers/rest/index.js';
const { QueryTypes } = require('sequelize');

var admin = require("firebase-admin");
var serviceAccount = require("../serviceaccount.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://lten-cf402.firebaseio.com"
});

const socketio = require('socket.io');

const PORT = process.env.PORT || 8080;

const server = restify.createServer({
  name: "ten",
  version: "1.0.0"
});

var io = socketio.listen(server.server);

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ["*"],
  allowHeaders: ["Authorization", "authorization"]
});

// register middleware
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
// server.use(
//   jwt({
//     secret: process.env.AUTH0_JWT_SECRET,
//     audience: "https://improve.chat",
//     issuer: "https://dev-w75mpftg.eu.auth0.com/"
//   }).unless({ path: ["/", "/webhooks/smooch"] })
// );

// register routes
server.get("/", rest.getHome);

server.get('/storage/*', restify.plugins.serveStaticFiles('./storage/'));

server.get("/teams", rest.getTeams);
server.get("/qr", rest.getQRScore);

server.get("/answers", rest.getAnswers);
server.post('/answer', rest.postAnswer);
server.put("/team/:id", rest.putTeam);

server.post('/notify', rest.notify);

server.get('/score', rest.getScore);
server.get('/events', rest.getEvents);
server.post('/bumpphase', rest.postBumpPhase);
server.post('/code', rest.postCode);
server.post('/manual', rest.postManual);

server.post('/genroute', rest.postGenRoute);



async function getRecentLocations() {
  let res = await models.sequelize.query('select t."name", t.image,i."deviceId",lat,lng from (select "deviceId",max("createdAt") cat from "LocationUpdates" lu group by "deviceId") i inner join "LocationUpdates" l on (i.cat = l."createdAt" AND l."deviceId" = i."deviceId") inner join "Teams" t on (t."deviceId" = i."deviceId") where l."appId" = \'' + process.env.APP_ID + '\';', { type: QueryTypes.SELECT });
  return res;
}

function socketlog(message, data, deviceId = false) {
  var today = new Date().toLocaleTimeString('nl-NL', { hour12: false, hour: '2-digit', minute: '2-digit' });
  if (!deviceId) {
    console.log("[" + today + "]" + message, data);
  } else {
    console.log("[" + deviceId.substr(0, 4) + "==" + today + "]" + message, data);
  }
}

io.sockets.on('connection', async function (socket) {
  socketlog("New client connected", {});

  io.emit('team.location.update', await getRecentLocations());

  socket.on('get.location.update', async function (data) {
    socketlog('sending manual location update');
    io.emit('team.location.update', await getRecentLocations());
  });

  socket.on('post.location.update', async function (data) {
    models.LocationUpdate.create({
      id: uuid.v4(),
      lat: data.lat,
      lng: data.lng,
      deviceId: data.deviceId,
      appId: process.env.APP_ID
    });
    // socketlog(`Received an update from device ${data.lat}\t${data.lng}`, '', data.deviceId);
    io.emit('team.location.update', await getRecentLocations());
  });
});

server.get("/force", async function (req, res, next) {
  socketlog('emmitting');
  io.emit('team.location.update', await getRecentLocations());
  res.send(204);
  next();
});

server.post('/refresh', function (req, res, next) {
  const { deviceId } = req.body;
  console.log(`========================> REFRESH ${deviceId}  <========================`);
  io.emit('refresh', { deviceId: { deviceId } });
});

server.post("/message", function (req, res, next) {
  socketlog('emmitting');
  io.emit('message', JSON.stringify({ message: req.body.message || 'test' }));
  res.send(204);
  next();
});

server.post("/finish", function (req, res, next) {
  socketlog('emmitting');
  io.emit('place.finish.marker', JSON.stringify({
    id: req.body.id || 1, lat: req.body.lat || 50.01
    , lng: req.body.lng || 5.01
  }));
  res.send(204);
  next();
});

server.listen(PORT, () =>
  socketlog("%s listening at %s", server.name, server.url)
);
